from django.shortcuts import render, redirect, get_object_or_404
from .forms import TaskForm, TaskNotesForm
from .models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
    }
    return render(request, "tasks/my_tasks.html", context)


@login_required
def add_task_notes(request):
    if request.method == "POST":
        form = TaskNotesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskNotesForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/task_notes.html", context)


@login_required
def view_task_notes(request, id):
    get_task = get_object_or_404(Task, id=id)
    context = {
        "get_task": get_task
    }
    return render(request, "tasks/view_notes.html", context)
