from django.db import models
from projects.models import Project
from django.conf import settings

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200, null=True)
    start_date = models.DateTimeField(null=True)
    due_date = models.DateTimeField(null=True)
    is_completed = models.BooleanField(default=False, null=True)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


class TaskNote(models.Model):
    task = models.ForeignKey(
        Task,
        related_name="tasknotes",
        on_delete=models.CASCADE,
        null=True,
    )
    notes = models.TextField(null=True)

    def __str__(self):
        return self.notes
