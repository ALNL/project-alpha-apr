from django import forms
from .models import Task, TaskNote


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]


class TaskNotesForm(forms.ModelForm):
    class Meta:
        model = TaskNote
        fields = [
            "notes",
            "task",
        ]
