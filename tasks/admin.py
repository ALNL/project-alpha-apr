from django.contrib import admin
from .models import Task, TaskNote

# Register your models here.


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
        "id",
    )


@admin.register(TaskNote)
class TaskNoteAdmin(admin.ModelAdmin):
    list_display = (
        "notes",
        "id",
    )
