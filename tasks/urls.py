from django.urls import path
from .views import create_task, show_my_tasks, add_task_notes, view_task_notes


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("notes/", add_task_notes, name="task_notes"),
    path("notes/view/<int:id>/", view_task_notes, name="view_notes"),
]
