import requests
from bs4 import BeautifulSoup


headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
}


def get_html(url):
    response = requests.get(url,headers=headers).text
    return response


def get_info(html):
    soup = BeautifulSoup(html,"lxml")
    name = soup.find("div",class_="er")     # tag name and class that contains the searched company name
    print(name)

if __name__ = "__main__":
    url = https://docs.djangoproject.com/en/4.1/topics/
    html = get_html(url)
    get_info(html)

